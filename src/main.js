import './firebase'
import Vue from 'vue'
import App from './App.vue'
import {firestorePlugin} from 'vuefire'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueRouter from 'vue-router'
import routes from './routes';
// Vue.config.productionTip = false;

Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(firestorePlugin);
 
new Vue({
  el: '#app',
  render: h => h(App),
})
